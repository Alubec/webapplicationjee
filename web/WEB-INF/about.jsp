<%--
  Created by IntelliJ IDEA.
  User: Łukasz
  Date: 2017-04-18
  Time: 11:44
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
<head>
    <title>Dig IT! - O stonie</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="${pageContext.request.contextPath}/WEB-INF/bootstrap.min.css" type="text/css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/WEB-INF/style.css" type="text/css" rel="stylesheet">
</head>

<body>

<nav class = "navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <a href="#" class="navbar-brand">Dig IT!</a>

        <button class="navbar-toggle" data-toggle="collapse" data-target=".navHeaderCollapse">
            <span class="glyphicon glyphicon-list"></span>
        </button>

        <div class="collapse navbar-collapse navHeaderCollapse">
            <ul class="nav navbar-nav navbar-right">
                <li class="active"><a href="#">Strona Główna</a></li>
                <li><a href="#">Dodaj</a></li>
                <li><a href="#">Zaloguj się</a></li>
                <li><a href="#">O stronie</a></li>
            </ul>
        </div>

    </div>
</nav>

<div class="container">
    <div class="jumbotron">
        <h1>Discover IT!</h1>
        <p>Strona z najciekawszymi znaleziskami dotyczącymi branży IT. Najświeższa porcja informacji branżowych. TODO:dopisać opis strony</p>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="navbar-text">Dig IT! - developed by Łukasz Cebula</p>
    </div>
</footer>

<script src="http://code.jquery.com/jquery-1.11.2.min.js"></script>
<script src="http://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
<script src="../resources/js/bootstrap.js"></script>
</body>
</html>

