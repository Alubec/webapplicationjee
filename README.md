# Web service #

Web service - aplikacja napisana w języku Java. Kompletna, działająca i użyteczna strona internetowa oparta o poznane technologie: HTML, CSS, Bootstrap, JAVA, Maven,
MySQL, JSP, Spring, GIT, Tomcat

Użytkownicy będą mieli możliwość zarejestrowania się, a ci, którzy się zalogują będą mogli dodawać "znaleziska" w branży IT. Znaleziska będą mogły być oceniane przez innych użytkowników, dzięki czemu będą decydowali, które treści powinny być wyświetlane wyżej w rankingu na stronie głównej.


### Funkcjonalności ###

- rejestracja
- logowanie
- dodawanie nowych treści
- przegląd dodanych treści 
- system głosowania