package pl.lukaszcebula.model;

/**
 * Created by Łukasz on 2017-04-21.
 */


import java.sql.Timestamp;

/**
 * Klasa Discovery przechowuje następujące informacje na temat "znaleziska": id, nazwa, opis, adres URL, czas dodania.
 * Dodatkowo pola dotyczące głosów na TAK i głosów na NIE
 * + referencje do obiektu użytkownika który dodał znalezisko(w tabeli wystpuje tylko id)
 */
public class Discovery {
    private long id;
    private String name;
    private String description;
    private String url;
    private Timestamp timestamp;
    private User user;
    private int upVote;
    private int downVote;

    public Discovery() {}

    public Discovery(Discovery discovery) {
        this.id = discovery.id;
        this.name = discovery.name;
        this.description = discovery.description;
        this.url = discovery.url;
        this.timestamp = new Timestamp(discovery.timestamp.getTime());
        this.user = new User(discovery.user);
        this.upVote = discovery.upVote;
        this.downVote = discovery.downVote;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public int getUpVote() {
        return upVote;
    }

    public void setUpVote(int upVote) {
        this.upVote = upVote;
    }

    public int getDownVote() {
        return downVote;
    }

    public void setDownVote(int downVote) {
        this.downVote = downVote;
    }

    @Override
    public String toString() {
        return "Discovery{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", url='" + url + '\'' +
                ", timestamp=" + timestamp +
                ", user=" + user +
                ", upVote=" + upVote +
                ", downVote=" + downVote +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Discovery discovery = (Discovery) o;

        if (id != discovery.id) return false;
        if (upVote != discovery.upVote) return false;
        if (downVote != discovery.downVote) return false;
        if (name != null ? !name.equals(discovery.name) : discovery.name != null) return false;
        if (description != null ? !description.equals(discovery.description) : discovery.description != null)
            return false;
        if (url != null ? !url.equals(discovery.url) : discovery.url != null) return false;
        if (timestamp != null ? !timestamp.equals(discovery.timestamp) : discovery.timestamp != null) return false;
        return user != null ? user.equals(discovery.user) : discovery.user == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (url != null ? url.hashCode() : 0);
        result = 31 * result + (timestamp != null ? timestamp.hashCode() : 0);
        result = 31 * result + (user != null ? user.hashCode() : 0);
        result = 31 * result + upVote;
        result = 31 * result + downVote;
        return result;
    }
}
