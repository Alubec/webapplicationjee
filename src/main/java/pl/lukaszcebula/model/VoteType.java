package pl.lukaszcebula.model;

/**
 * Created by Łukasz on 2017-04-21.
 */


/**
 * Typ głosu reprezentowany typem enum.
 */


public enum VoteType {
        VOTE_UP, VOTE_DOWN;
    }
