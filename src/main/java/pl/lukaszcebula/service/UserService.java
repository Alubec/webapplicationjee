package pl.lukaszcebula.service;

import pl.lukaszcebula.dao.DAOFactory;
import pl.lukaszcebula.dao.UserDAO;
import pl.lukaszcebula.model.User;

/**
 * Created by Łukasz on 2017-04-30.
 */

/**
 *  metoda addUser(), jest odpowirdzialna za utworzenie obiektu klasy User i zapisanie odpowiednich danych w bazie.
 *  Argumenty metody będą pochodziły z żądania przesłanego do odpowiedniego kontrolera.
 */
public class UserService {
    public void addUser(String username, String email, String password) {
        User user = new User();
        user.setUsername(username);
        user.setPassword(password);
        user.setEmail(email);
        user.setActive(true);
        DAOFactory factory = DAOFactory.getDAOFactory();
        UserDAO userDao = factory.getUserDAO();
        userDao.create(user);
    }

    public User getUserById(long userId) {
        DAOFactory factory = DAOFactory.getDAOFactory();
        UserDAO userDao = factory.getUserDAO();
        User user = userDao.read(userId);
        return user;
    }

    public User getUserByUsername(String username) {
        DAOFactory factory = DAOFactory.getDAOFactory();
        UserDAO userDao = factory.getUserDAO();
        User user = userDao.getUserByUsername(username);
        return user;
    }
}