package pl.lukaszcebula.dao;


import pl.lukaszcebula.model.User;
import pl.lukaszcebula.util.ConnectionProvider;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

/**
 * Created by Łukasz on 2017-04-30.
 */

/**
 * W klasie zdefiniowano stałą z szablonem zapytania dodającego nowego użytkownika.
 * Pola, które powinien posiadać dodawany do bazy obiekt klasy User to username, email, password i active.
 * Korzystam z nazwanych parametrów poprzedzonych dwukropkiem, ponieważ wykorzystuje dalej obiekt
 * klasy ze Spring JDBC o nazwie NamedParameterJdbcTemplate
 */
public class UserDAOImpl implements UserDAO {

    private static final String CREATE_USER =
            "INSERT INTO user(username, email, password, is_active) VALUES(:username, :email, :password, :active);";
    private static final String READ_USER =
            "SELECT user_id, username, email, password, is_active FROM user WHERE user_id = :id";
    private static final String READ_USER_BY_USERNAME =
            "SELECT user_id, username, email, password, is_active FROM user WHERE username = :username";

    private NamedParameterJdbcTemplate template;

    public UserDAOImpl() {
        template = new NamedParameterJdbcTemplate(ConnectionProvider.getDataSource());
    }


    /**
     *
     * Metoda create() przyjmuje jako argument obiekt klasy User.
     *
     */
    @Override
    public User create(User user) {
        User resultUser = new User(user);
        KeyHolder holder = new GeneratedKeyHolder();
        SqlParameterSource paramSource = new BeanPropertySqlParameterSource(user);
        int update = template.update(CREATE_USER, paramSource, holder);
        if(update > 0) {
            resultUser.setId((Long)holder.getKey());
            setPrivigiles(resultUser);
        }
        return resultUser;
    }

    private void setPrivigiles(User user) {
        final String userRoleQuery = "INSERT INTO user_role(username) VALUES(:username)";
        SqlParameterSource paramSource = new BeanPropertySqlParameterSource(user);
        template.update(userRoleQuery, paramSource);
    }


    /**
     *
     * metoda read() wyszukuje użytkownika na podstawie jego id, czyli klucza głównego tabeli user.
     * Tworzy się więc odpowiedni obiekt MapSqlParameterSource
     * z przypisaniem wartości primaryKey do id, które następnie wstawione zostaną do zapytania READ_USER.
     */
    @Override
    public User read(Long primaryKey) {
        User resultUser = null;
        SqlParameterSource paramSource = new MapSqlParameterSource("id", primaryKey);
        resultUser = template.queryForObject(READ_USER, paramSource, new UserRowMapper());
        return resultUser;
    }

    @Override
    public boolean update(User updateObject) {
        return false;
    }

    @Override
    public boolean delete(Long key) {
        return false;
    }

    @Override
    public List<User> getAll() {
        return null;
    }


    /**
     *
     * Metoda getUserByUsername() jako warunek
     * w zapytaniu wykorzystuje unikalną nazwę użytkownika(username), a nie jego id.
     */
    @Override
    public User getUserByUsername(String username) {
        User resultUser = null;
        SqlParameterSource paramSource = new MapSqlParameterSource("username", username);
        resultUser = template.queryForObject(READ_USER_BY_USERNAME, paramSource, new UserRowMapper());
        return resultUser;
    }

    /**
     * Do utworzenia wynikowego obiektu tworzy się pomocnicza klasa wewnętrzna UserRowMapper
     * implementująca interfejs RowMapper. Posiada on tylko jedną metodę mapRow(), w której tworzy się obiekt User
     * i ustawiamy jego kolejne pola poprzez settery.
     * Wartości pobieramy z obiektu ResultSet, w którym do odpowiednich kolumn możemy odwoływać się po ich nazwach
     * lub indeksach.
     */
    private class UserRowMapper implements RowMapper<User> {

        @Override
        public User mapRow(ResultSet resultSet, int rowNum) throws SQLException {
            User user = new User();
            user.setId(resultSet.getLong("user_id"));
            user.setUsername(resultSet.getString("username"));
            user.setEmail(resultSet.getString("email"));
            user.setPassword(resultSet.getString("password"));
            return user;
        }

    }
}




















