package pl.lukaszcebula.dao;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Łukasz on 2017-04-30.
 */


/**
 * interfejs GenericDAO udostępnia metody CRUD oraz dodatkową metodę getAll() zwracającą wszystkie wyniki z danej tabeli.
 *
 */
public interface GenericDAO <T, PK extends Serializable> {

    //CRUD
    T create (T newObject);
    T read(PK primaryKey);
    boolean update(T updateObject);
    boolean delete(PK key);
    List<T> getAll();

}
