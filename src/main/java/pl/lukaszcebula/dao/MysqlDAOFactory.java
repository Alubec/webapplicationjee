package pl.lukaszcebula.dao;

/**
 * Created by Łukasz on 2017-04-30.
 */

public class MysqlDAOFactory extends DAOFactory {

    @Override
    public DiscoveryDAO getDiscoveryDAO() {
        return new DiscoveryDAOImpl();
    }

    @Override
    public UserDAO getUserDAO() {
        return new UserDAOImpl();
    }

    @Override
    public VoteDAO getVoteDAO() {
        return new VoteDAOImpl();
    }

}
