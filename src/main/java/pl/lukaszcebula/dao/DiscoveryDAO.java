package pl.lukaszcebula.dao;

import pl.lukaszcebula.model.Discovery;

import java.util.List;

/**
 * Created by Łukasz on 2017-04-30.
 */

public interface DiscoveryDAO extends GenericDAO<Discovery, Long> {

    List<Discovery> getAll();

}
