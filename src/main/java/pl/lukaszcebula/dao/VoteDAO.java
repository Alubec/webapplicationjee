package pl.lukaszcebula.dao;

import pl.lukaszcebula.model.Vote;

/**
 * Created by Łukasz on 2017-04-30.
 */

public interface VoteDAO extends GenericDAO<Vote, Long> {

    public Vote getVoteByUserIdDiscoveryId(long userId, long discoveryId);

}
