package pl.lukaszcebula.dao;

import pl.lukaszcebula.model.User;

import java.util.List;

/**
 * Created by Łukasz on 2017-04-30.
 */

public interface UserDAO extends GenericDAO<User, Long> {

    List<User> getAll();
    User getUserByUsername(String username);

}
