package pl.lukaszcebula.exception;

/**
 * Created by Łukasz on 2017-05-02.
 */

/**
 * wyjątek informujący o podaniu niepoprawnego rodzaju bazy danych,
 *
 */
public class NoSuchDbTypeException extends Exception {

    private static final long serialVersionUID = 1L;

}
