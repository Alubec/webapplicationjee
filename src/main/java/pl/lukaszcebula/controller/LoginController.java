package pl.lukaszcebula.controller;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServlet;
import java.io.IOException;

/**
 * Created by Łukasz on 2017-05-03.
 */

/**
 * LoginController, mapowanie ustawiono na adres /login.
 * rozpoznaj sytuację, czy użytkownik jest zalogowany
 * i jeżeli faktycznie tak jest, to przekierowuje go do strony głównej, natomiast jeżeli
 * nie jest zalogowany wyświetlam stronę błędu 403.
 */
@WebServlet("/login")
public class LoginController extends HttpServlet {
    private static final long serialVersionUID = 1L;

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        if (request.getUserPrincipal() != null) {
            response.sendRedirect(request.getContextPath());
        } else {
            response.sendError(403);
        }
    }
}
