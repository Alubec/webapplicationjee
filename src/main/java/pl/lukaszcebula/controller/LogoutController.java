package pl.lukaszcebula.controller;


import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by Łukasz on 2017-05-03.
 */

/**
 * Wylogowanie użytkownika sprowadza się do usunięcia sesji danego użytkownika przy pomocy metody invalidate()
 * obiektu HttpSession. Po tej operacji przekierowujemy użytkownika do strony głównej.
 * Wylogowywanie zdefiniowano pod adresem /logout.
 */
@WebServlet("/logout")
public class LogoutController extends HttpServlet {
    private static final long serialVersionUID = 1L;

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.getSession().invalidate();
        response.sendRedirect(request.getContextPath());
    }
}