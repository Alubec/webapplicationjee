package pl.lukaszcebula.filter;

import pl.lukaszcebula.model.User;
import pl.lukaszcebula.service.UserService;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;

/**
 * Created by Łukasz on 2017-05-03.
 */


/**
 * Filtr posiada mapowanie, które sprawia, że metoda doFilter() wykona się przed przejściem do jakiejkolwiek strony.
 * Sprawdzam w niej bardzo warunek i jeżeli użytkownik się zalogował(metoda getUserPrincipal()
 * zwraca wartość różną od null) oraz jednocześnie obiekt user na poziomie sesji nie istnieje, to go tam
 * zapisujem pobierając użytkownika z bazy danych na podstawie znanej nazwy użytkownika.
 * Następnie żądanie przekazywane jest dalej do adresu docelowego.
 */
@WebFilter("/*")
public class LoginFilter implements Filter {

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest httpReq = (HttpServletRequest) request;
        if(httpReq.getUserPrincipal() != null && httpReq.getSession().getAttribute("user") == null) {
            saveUserInSession(httpReq);
        }
        chain.doFilter(request, response);
    }

    private void saveUserInSession(HttpServletRequest request) {
        UserService userService = new UserService();
        String username = request.getUserPrincipal().getName();
        User userByUsername = userService.getUserByUsername(username);
        request.getSession(true).setAttribute("user", userByUsername);
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void destroy() {
    }

}