package pl.lukaszcebula.util;

/**
 * Created by Łukasz on 2017-04-21.
 */

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * Klasa umożliwia pobranie źródła danych (DataSource) skonfigurowanego w pliku context.xml
 * oraz pojedyncze połączenie
 */
public class ConnectionProvider {

    private static DataSource dataSource;

    public static Connection getConnection() throws SQLException {
        return getDataSource().getConnection();
    }

    public static DataSource getDataSource() {
        if (dataSource == null) {
            try {
                Context initialContext = new InitialContext();
                Context envContext = (Context) initialContext.lookup("java:comp/env");

                DataSource ds = (DataSource) envContext.lookup("jdbc/digit");
                dataSource = ds;
            } catch (NamingException e) {
                e.printStackTrace();
            }
        }
        return dataSource;
    }
}
